Aeros Character Handler Project
===============================

Informations
------------

- @author : Clément "DrGrokit" Barde
- @version : 0.0

Etat
----

Base structure not complete.

Fichiers disponibles
--------------------

- aeros.character :
  - Class Alignement : enum for possibles alignements
  - Class Character : Main class for character
  - Class Stats : define stats set

- aeros.cls :
  - Class PlayableClass : interface for playable classes

- aeros.races :
  - Class Race : interface for playable races

- aeros :
  - Class CharacterCreatorMain : class running the main program
