package aeros.cls;

/**
  * Main class for Character
  *
  * @author Clément "DrGrokit" Barde
  */
public interface PlayableClass {
  protected String className;
  protected String advancedName;
  protected List<String> advancedWays;
  protected String currentState;
  protected String mecanicName;
  // Set a mecanicState, type maybe 'int' or 'List' according to class

  /**
    * Returns the bonus/malus of HP given by this class
    *
    * @return Amount of HP
    */
  public int bonusHP();


}
