package aeros.races;

/**
  * Interface defining races
  *
  * @author Clément "DrGrokit" Barde
  */
public interface Race {
  protected Collection<PlayableClass> eligibleClasses;
  protected String raceName;

  /**
    * Returns the list of classes that can be played by this race
    *
    * @return The list of classes playable by this race
    */
  public Collection<PlayableClasse> canBe();

  /**
    * Returns the base stat set for this race
    *
    * @return Base stat set for this class
    */
  public Stats baseStats();

  /**
    * Returns the base HP amounts for this class
    *
    * @return Base HP amount for this class
    */
  public int baseHP();

  /**
    * Returns true iff the given class is playble by this race
    *
    * @param cls A class
    * @return - true iff this race can be the given class
    *         - false otherwise
    */
  public boolean isPlayable(Class cls);

  /**
    * Returns the race name
    *
    * @return this race name
    */
  public String getName();
}
