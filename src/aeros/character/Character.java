package aeros.character;

/**
  * Main class for Character
  *
  * @author Clément "DrGrokit" Barde
  */
public class Character {
  protected String name; // Character's name
  protected Race race; // Character's race
  protected Collection<PlayableClass> clss; // Character's classes
  protected int xp; // Current xp
  protected int lvl; // Current lvl
  protected Alignement ali; // Character's alignement
  protected String background; // Character's background (optionnal)
  protected int age; // Character's age
  protected String desc; // Character description
  protected Stats stats; // Statistics

  /**
    * Character constructor.
    * Indicate your name, all other stat wil be set to default.
    * You must set them with the rights methods
    *
    * @param name Your character's name
    */
  public Character(String name) {
    this.name = name;
    this.race = new RaceHuman();
    this.clss = new ArrayList<PlayableClasse>();
    this.xp = 0;
    this.lvl = 1;
    this.ali = Alignement.NotSet;
    this.background = "Here will be your awesome story.";
    this.age = 0;
    this.desc = "Tell us how crazy you look.";
    this.stats = this.race.baseStats();
  }
}
