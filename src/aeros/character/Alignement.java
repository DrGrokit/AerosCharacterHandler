package aeros.character;

/**
  * All possible alignements
  *
  * @author Clément "DrGrokit" Barde
  */
public enum Alignement {
  NotSet(0),
  LoyalBon(1),
  LoyalNeutre(2),
  LoyalMauvais(3),
  NeutreBon(4),
  NeutreNeutre(5),
  NeutreMauvais(6),
  ChaoticBon(7),
  ChaoticNeutre(8),
  ChaoticMauvais(9);
}
