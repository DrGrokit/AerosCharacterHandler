package aeros.character;

/**
  * Class for Characters' stats
  * They are currently 9 stats :
  *   - Strength (force);
  *   - Dexterity (dexterité);
  *   - Constitution (constitution);
  *   - Intelligence (intelligence);
  *   - Wisdom (sagesse);
  *   - Charisma (charisme);
  *   - Initiative (initiative);
  *   - Calm/self-control (calme/control de soi);
  *   - Luck (chance).
  *
  * A stat is included in [0,89], excpet from luck that is in [0,99]
  * Base stats are determined by your race and class.
  *
  * @author Clément "DrGrokit" Barde
  */
public class Stats {
  private int str;
  private int dex;
  private int con;
  private int itl;
  private int wis;
  private int cha;
  private int ini;
  private int ctr;
  private int luc;

  /**
    * Default stats constructor
    * Create a set of stat initialized at 50
    */
  public Stats() {
    this.str = 50;
    this.dex = 50;
    this.con = 50;
    this.itl = 50;
    this.wis = 50;
    this.cha = 50;
    this.ini = 50;
    this.ctr = 50;
    this.luc = 50;
  }

  /**
    * Set Strength stat
    *
    * @param val New stat value
    */
  public void setStrength(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.str = val;
    }
  }

  /**
    * Set Dexterity stat
    *
    * @param val New stat value
    */
  public void setDexterity(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.dex = val;
    }
  }

  /**
    * Set Constitution stat
    *
    * @param val New stat value
    */
  public void setConstitution(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.con = val;
    }
  }

  /**
    * Set Intelligence stat
    *
    * @param val New stat value
    */
  public void setIntelligence(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.itl = val;
    }
  }

  /**
    * Set Wisdom stat
    *
    * @param val New stat value
    */
  public void setWisdom(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.wis = val;
    }
  }

  /**
    * Set Charisma stat
    *
    * @param val New stat value
    */
  public void setCharisma(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.cha = val;
    }
  }

  /**
    * Set Initiative stat
    *
    * @param val New stat value
    */
  public void setInitiative(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.ini = val;
    }
  }

  /**
    * Set Calm stat
    *
    * @param val New stat value
    */
  public void setCalm(int val) {
    if((val>89)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.ctr = val;
    }
  }

  /**
    * Set Luck stat
    *
    * @param val New stat value
    */
  public void setLuck(int val) {
    if((val>99)||(val<0)) {
      Sytem.out.println("Incorrect value");
    }
    else {
      this.luc = val;
    }
  }
}
